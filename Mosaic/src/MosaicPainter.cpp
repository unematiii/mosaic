#include <QImage>
#include <QFile>
#include <QXmlStreamReader>

#include <functional>
#include <cmath>

#include "src/ImageSetBuilder.h"
#include "src/MosaicPainter.h"

#include "src/misc/Grid.h"

void MosaicPainter::parseImageSet()
{
    QFile file(getImageSetFile());
    file.open(QFile::ReadOnly);
    QXmlStreamReader xml(&file);

    while(!xml.atEnd() && !xml.hasError()) {
        /* Read next element.*/
        QXmlStreamReader::TokenType token = xml.readNext();

        /* If token is just StartDocument, we'll go to next.*/
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }

        /* If token is StartElement, we'll see if we can read it.*/
        if(token == QXmlStreamReader::StartElement) {

            /* If it's named images, we'll go to the next.*/
            if(xml.name() == "images") {
                continue;
            }

            /* If it's named image, we'll dig the information from there.*/
            if(xml.name() == "image") {
                ImageSetItem item = this->parseImage(xml);

                m_images.push_back(item);
                m_points.push_back(
                    Point(qRed(item.getColor()),qGreen(item.getColor()),qBlue(item.getColor()))
                );
            }
        }
    }

    /* Add items to trees */
    for (size_t i = 0; i < m_points.size(); i++)
        m_trees[m_images[i].size()].add(&m_points[i], &m_images[i]);

    /* Build trees */
    for(TreeMap::iterator it = m_trees.begin(); it != m_trees.end(); it++)
        (*it).second.build();

    /* Error handling. */
    if(xml.hasError()) {
        qDebug() << "MosaicPainter::MosaicPainter(): " << xml.errorString();
        emit error(xml.errorString());
    }

    /* Removes any device() or data from the reader
     * and resets its internal state to the initial state. */
    xml.clear();
}

MosaicPainter::MosaicPainter(QObject *parent)
    : QObject(parent),
      m_mosaic(0),
      m_future_watcher(new QFutureWatcher<void>()),
      m_tiles(new QList<QRect>),
      m_grid(0)
{
    /* Set up future watcher */
    connect(m_future_watcher, SIGNAL(progressValueChanged(int)),
            this, SLOT(emitProgressValueChanged(int)));

    connect(m_future_watcher, SIGNAL(started()),
            this, SLOT(emitStarted()));

    connect(m_future_watcher, SIGNAL(finished()),
            this, SLOT(emitFinished()));
}

MosaicPainter::~MosaicPainter()
{
    if(m_future_watcher)
        delete m_future_watcher;

    if(m_tiles)
        delete m_tiles;

    if(m_grid)
        delete m_grid;

    if(m_mosaic)
        delete m_mosaic;
}

void MosaicPainter::paint()
{
    emit statusChanged("Matching tiles...");

    /* Get images */
    parseImageSet();

    qDebug() << "MosaicPainter::paint(): Pixels: "
             << m_mosaic->width() * m_mosaic->height();

    /* Process each cell in grid */
    m_tiles = m_grid->decompose();

    qDebug() << "MosaicPainter::paint(): # tiles to process: "
           << m_tiles->size();

    QFuture<void> future = QtConcurrent::map(
                *m_tiles, std::bind1st(std::mem_fun(&MosaicPainter::processRect), this)
    );
    m_future_watcher->setFuture(future);
}

ImageSetItem MosaicPainter::parseImage(QXmlStreamReader &xml)
{
    ImageSetItem item;
    /* Next element... */
    xml.readNext();
    /*
     * We're going to loop over the things because the order might change.
     * We'll continue the loop until we hit an EndElement named image.
     */
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "image")) {
        if(xml.tokenType() == QXmlStreamReader::StartElement) {
            /* We've found path. */
            if(xml.name() == "path") {
                /* Discard tag name */
                xml.readNext();
                /* Get path */
                item.setFilePath(xml.text().toString());
            }
            /* We've found size. */
            if(xml.name() == "size") {
                /* Discard tag name */
                xml.readNext();
                /* Get size */
                item.setSize(xml.text().toInt());
            }
            /* We've found color. */
            if(xml.name() == "color") {
                /* Discard tag name */
                xml.readNext();
                /* Get color */
                item.setColor(xml.text().toUInt());
            }
        }
        /* ...and next... */
        xml.readNext();
    }

    return item;
}

Grid *MosaicPainter::getGrid()
{
    return m_grid;
}

void MosaicPainter::setGrid(Grid* grid)
{
    m_grid = grid;
}

void MosaicPainter::processRect(QRect rect)
{
    /*
    qDebug() << "MosaicPainter::processRect(): Rect is "
             << rect;
    */

    /* Grab a copy of the region */
    QImage tile = m_mosaic->copy(rect);

    /* Find nearest match (kd-tree) */
    unsigned color = ImageSetBuilder::calculateAvgColor(tile);

    ImageSetItem best = *m_trees[tile.width()].nearest_iterative(
        Point(qRed(color),qGreen(color),qBlue(color))
    );

/*
    qDebug() << "MosaicPainter::processRect() : Best tile is colored "
           << best_tile.getColor();

    qDebug() << "MosaicPainter::processRect() : Best tile path "
           << best_tile.filePath();
*/

    /* Replace */
    QImage replacement(best.filePath());
    for(int x = rect.x(); x < rect.x() + rect.width(); x++)
        for(int y = rect.y(); y < rect.y() + rect.height(); y++)
            m_mosaic->setPixel(x, y, replacement.pixel(x - rect.x(), y - rect.y()));
}

void MosaicPainter::emitProgressValueChanged(int value)
{
    emit progressValueChanged(value);
}

void MosaicPainter::emitStarted()
{
    emit started(m_tiles->length());
}

void MosaicPainter::emitFinished()
{
    /* Save image */
    saveMosaic();

    emit finished();
}

void MosaicPainter::resetFields()
{
    qDebug() << "MosaicPainter::resetFields()";

    if(m_mosaic)
    {
        delete m_mosaic;
        m_mosaic = 0;
    }

    if(m_tiles)
    {
        m_tiles->clear();
    }

    m_trees.clear();
    m_points.clear();
    m_images.clear();

    if(m_grid)
    {
        delete m_grid;
        m_grid = 0;
    }

    m_target_image_file = "";
    m_source_image_file = "";
    m_imageset_file = "";
}

void MosaicPainter::saveMosaic()
{
    m_mosaic->save(m_target_image_file);
}
QImage *MosaicPainter::mosaic() const
{
    return m_mosaic;
}

void MosaicPainter::setMosaic(QImage *mosaic)
{
    m_mosaic = mosaic;
}

QString MosaicPainter::getSourceImageFile() const
{
    return m_source_image_file;
}

void MosaicPainter::setSourceImageFile(const QString &file)
{
    m_source_image_file = file;
}

QString MosaicPainter::getTargetImageFile() const
{
    return m_target_image_file;
}

void MosaicPainter::setTargetImageFile(const QString &file)
{
    m_target_image_file = file;
}

QString MosaicPainter::getImageSetFile() const
{
    return m_imageset_file;
}

void MosaicPainter::setImageSetFile(const QString &file)
{
    m_imageset_file = file;
}
