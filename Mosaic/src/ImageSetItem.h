#ifndef IMAGESETITEM_H
#define IMAGESETITEM_H

class ImageSetItem
{
public:
    ImageSetItem()
    {
    }

    ImageSetItem(const QString& file, int size)
        : m_file_path(file), m_size(size)
    {
    }

    QString filePath() const
    {
        return m_file_path;
    }

    void setFilePath(const QString &filePath)
    {
        m_file_path = filePath;
    }

    int size() const
    {
        return m_size;
    }

    void setSize(int size)
    {
        m_size = size;
    }

    unsigned getColor() const
    {
        return m_color;
    }

    void setColor(unsigned color)
    {
        m_color = color;
    }

private:
    QString m_file_path;
    int m_size;
    unsigned m_color;

};

#endif // IMAGESETITEM_H
