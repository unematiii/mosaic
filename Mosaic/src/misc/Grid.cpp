#include "src/misc/Grid.h"

#include <cmath>
#include <cstdlib>
#include <ctime>

QList<QRect> *SimpleGrid::decompose()
{
    int cols = std::floor(m_width / m_grid_size);
    int rows = std::floor(m_height / m_grid_size);

    QList<QRect> *temp = new QList<QRect>();

    for(int i = 0; i < rows; i++)
        for(int j = 0; j < cols; j++)
            temp->append(
                QRect(j * m_grid_size, i * m_grid_size, m_grid_size, m_grid_size)
            );

    return temp;
}

ComplexGrid::ComplexGrid(unsigned width, unsigned height, unsigned min_size, unsigned max_size)
    : Grid(width, height),
      m_min_size(min_size),
      m_max_size(max_size),
      m_subsets(0),
      m_container(std::floor(width / max_size), std::floor(height / max_size), min_size, max_size),
      m_tolerance((double)DEFAULT_TOLERANCE/100)
{
}

ComplexGrid::~ComplexGrid()
{
}

void ComplexGrid::createGrid()
{
    /* Pick random points, identify their parents in Disjoint-Set data structure. For each parent
       count the number of occurences. If there exists parent that accounts up to DEFAULT_TOLERANCE %
       of random points - fix the tile. */

    qDebug() << "ComplexGrid::createGrid(): Tolerance is " << m_tolerance;

    /* Calculate number of iterations. NB! Im assuming that tile sizes are two to the power of smth &
       min < max . */
    int iter = 0;
    int max_iter = 0;
    for(unsigned t = m_max_size; t != m_min_size; ++max_iter)
        t = t >> 1;

    qDebug() << "ComplexGrid::decompose(): Number of iterations " << max_iter;

    /* Seed */
    std::srand(time(0));

    while(iter < max_iter)
    {
        /* For each cell */
        for(GridCell& cell : m_container.cells())
        {
            /*
            qDebug() << "ComplexGrid::decompose(): Observing cell at "
                     << QString("(%1, %2).").arg(cell.x).arg(cell.y);
            */
            /* Map for occurences <parent, count> */
            std::map<int, int> occurences;

            /* Is not fixed */
            if(!cell.inPlace())
            {
                /* # of random points - 50% pixels */
                int noOfRandomPoints = (cell.size * cell.size) / 2;
                for(int i = 0; i < noOfRandomPoints; i++)
                {
                    /* Random point inside this cell */
                    int x = cell.x + (std::rand() % cell.size);
                    int y = cell.y + (std::rand() % cell.size);

                    /* Map to element in DSS */
                    int element = (y * m_width +  x);

                    /*
                    qDebug() << "ComplexGrid::decompose(): Random point is "
                             << QString("(%1, %2)").arg(x).arg(y) << " which maps to " << element;
                    */
                    int parent = Subset::find(m_subsets, element);

                    /*
                    qDebug() << "ComplexGrid::decompose(): Parent of "
                             << element << " is " << parent;
                    */

                    occurences[parent] += 1;
                }

                for(std::map<int, int>::iterator it = occurences.begin();
                    it != occurences.end(); it++)
                {
                    /* Fix the tile */
                    if((*it).second >= m_tolerance * noOfRandomPoints)
                    {
                        cell.setInPlace(true);
                    }
                }
            }
        }

        /* Divide cells that are not in place */
        m_container.divide();
        iter++;
    }
}

QList<QRect> *ComplexGrid::decompose()
{
    QList<QRect> *temp = new QList<QRect>();
    for(GridCell& cell : m_container.cells())
        temp->push_back(QRect(cell.x, cell.y, cell.size, cell.size));

    return temp;
}

Subset **ComplexGrid::subsets() const
{
    return m_subsets;
}

void ComplexGrid::setSubsets(Subset **subsets)
{
    m_subsets = subsets;
}
double ComplexGrid::tolerance() const
{
    return m_tolerance;
}

void ComplexGrid::setTolerance(double tolerance)
{
    m_tolerance = tolerance;
}

QList<ComplexGrid::GridCell> ComplexGrid::GridCell::divide()
{
    /*
    qDebug() << "QList::divide(): Dividing cell at " << QString("(%1, %2).").arg(x).arg(y);
    */

    QList<GridCell> temp;

    for(int i = 0; i < 4; i++)
    {
        temp.append(GridCell(x, y, size/2, min_size));
        temp.append(GridCell(x + (size/2), y, size/2, min_size));
        temp.append(GridCell(x, y + (size/2), size/2, min_size));
        temp.append(GridCell(x + (size/2), y + (size/2), size/2, min_size));
    }

    return temp;
}
