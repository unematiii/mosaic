#include "src/misc/Graph.h"

#include <QDebug>

int Subset::find(Subset **subsets, int i)
{
    /* qDebug() << "Subset::find()"; */

    /* Find root and make root as parent of i (Path Compression) */
    if (subsets[i]->parent != i)
    {
        subsets[i]->parent = Subset::find(subsets, subsets[i]->parent);
    }

    /* qDebug() << "Subset::find(): Parent of " << QString("%1 is %2.").arg(i).arg(subsets[i]->parent); */

    return subsets[i]->parent;
}

int Subset::merge(Subset **subsets, int x, int y)
{
    /* qDebug() << "Subset::merge()"; */

    int xroot = Subset::find(subsets, x);
    int yroot = Subset::find(subsets, y);

    /*
    qDebug() << "Subset::merge(): size of a xroot: " << subsets[xroot]->count;
    qDebug() << "Subset::merge(): size of a yroot: " << subsets[yroot]->count;
    */

    /*  Attach smaller rank tree under root of high rank tree */
    /*  (Union by Rank) */
    if (subsets[xroot]->rank < subsets[yroot]->rank)
    {
        subsets[yroot]->count += subsets[xroot]->count;
        subsets[xroot]->parent = yroot;
        return yroot;
    }
    else if (subsets[xroot]->rank > subsets[yroot]->rank)
    {
        subsets[xroot]->count += subsets[yroot]->count;
        subsets[yroot]->parent = xroot;
        return xroot;
    }

    /*  If ranks are same, then make one as root and increment */
    /*  its rank by one */
    else
    {
        subsets[xroot]->count += subsets[yroot]->count;
        subsets[yroot]->parent = xroot;
        subsets[xroot]->rank++;
        return xroot;
    }
}

void Graph::insert(Graph::Edge e)
{
    m_edges.push_back(e);
}

unsigned Graph::getSize() const
{
    return m_size;
}

void Graph::setSize(const unsigned &size)
{
    m_size = size;
}

std::vector<Graph::Edge>& Graph::edges()
{
    return m_edges;
}

void Graph::setEdges(const std::vector<Edge> &edges)
{
    m_edges = edges;
}


