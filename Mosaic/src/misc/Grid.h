#ifndef GRID_H
#define GRID_H

#include <QDebug>
#include <QList>
#include <QRect>

#include "src/misc/Graph.h"

class Grid
{
public:
    Grid(unsigned width, unsigned height)
        : m_width(width), m_height(height)
    {
    }

    virtual QList<QRect> * decompose() = 0;
    virtual ~Grid() {}

protected:
    unsigned m_width;
    unsigned m_height;

};

class SimpleGrid : public Grid
{
public:
    static const int DEFAULT_GRID_SIZE = 0x20;

    /**
     * @brief Grid
     * @param width Width of cropped image that can hold 'grid_size' sized tiles.
     * @param height Height of cropped image that can hold 'grid_size' sized tiles.
     * @param grid_size Cells of the grid are sized grid_size x grid_size px.
     */
    SimpleGrid(unsigned width, unsigned height, unsigned grid_size)
        : Grid(width, height), m_grid_size(grid_size)
    {}

    QList<QRect> * decompose();

private:
    unsigned m_grid_size;

};

class ComplexGrid : public Grid
{
public:
    static const int DEFAULT_TOLERANCE = 85;

    enum GRID_TILE_SIZE { MIN = 0x8, MAX = 0x40 };

    class GridCell
    {
    public:
        GridCell(int x, int y, int size, unsigned min_size)
            : x(x), y(y), size(size), min_size(min_size), in_place(false)
        {
        }

        bool canDivide()
        {
            return (!in_place && size > min_size);
        }

        void setInPlace(bool status)
        {
            /*
            if(status)
                qDebug() << "QList::setInPlace(): Cell at " << QString("(%1, %2) %3 x %3").arg(x).arg(y).arg(size)
                         << " is in place.";
            */
            in_place = status;
        }

        bool inPlace()
        {
            return in_place;
        }

        QList<GridCell> divide();

        friend class ComplexGrid;

    private:
        int x, y;
        unsigned size;
        unsigned min_size;
        bool in_place;
    };

    class GridCellContainer
    {
    public:
        GridCellContainer(int cols, int rows, unsigned min_size, unsigned max_size)
            : m_cols(cols), m_rows(rows)
        {
            for(int i = 0; i < rows; i++)
                for(int j = 0; j < cols; j++)
                    m_cells.push_back(GridCell(j * max_size, i * max_size, max_size, min_size));
        }

        QList<GridCell>& cells()
        {
            return m_cells;
        }

        void divide()
        {
            /*
            qDebug() << "GridCellContainer::divide()";
            */
            QList<GridCell> temp;

            for(GridCell& cell : m_cells)
            {
                if(cell.canDivide())
                    for(GridCell t : cell.divide())
                        temp.push_back(t);
                else
                    temp.push_back(cell);
            }

            m_cells = temp;
        }

    private:
        QList<GridCell> m_cells;
        int m_cols;
        int m_rows;

    };

    /**
     * @brief Grid
     * @param width Width of cropped image that can hold 'MAX' sized tiles.
     * @param height Height of cropped image that can hold 'MAX' sized tiles.
     * @param subsets Array of subsets
     */
    ComplexGrid(unsigned width, unsigned height, unsigned min_size, unsigned max_size);
    ~ComplexGrid();

    QList<QRect> * decompose();

    Subset **subsets() const;
    void setSubsets(Subset **subsets);

    double tolerance() const;
    void setTolerance(double tolerance);

    void createGrid();

private:
    unsigned m_min_size;
    unsigned m_max_size;
    Subset **m_subsets;
    GridCellContainer m_container;
    double m_tolerance;

};

#endif // GRID_H
