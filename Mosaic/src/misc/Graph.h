#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <vector>
#include <cstring>

class Graph
{
public:
    struct Edge
    {
        double weight;
        int a;
        int b;

        Edge(double weight, int a, int b)
            : weight(weight), a(a), b(b)
        {}

        bool operator <(const Edge& e) const
        {
            return weight < e.weight;
        }
    };

    Graph(unsigned size)
        : m_size(size)
    {
    }

    virtual ~Graph()
    {
        m_edges.clear();
    }

    void insert(Edge e);

    unsigned getSize() const;
    void setSize(const unsigned &size);

    std::vector<Edge>& edges();
    void setEdges(const std::vector<Edge> &edges);

private:
    unsigned m_size;
    std::vector<Edge> m_edges;

};

struct Subset
{
    double Int;
    int parent;
    int rank;
    int count;
    int key;

    Subset(double Int, int parent, int rank, int key)
        : Int(Int), parent(parent), rank(rank), count(1), key(key)
    {
    }

    /**
     * @brief find
            Returns the root index of element i
     * @param subsets
            All components
     * @param i
            Element index
     * @return
            Root index of element i
     */
    static int find(Subset **subsets, int i);

    /**
     * @brief merge
            Merges two components into one
     * @param subsets
            Components
     * @param x
            Element of component C1
     * @param y
            Element of component C2
     * @return
            Index of the new root element
     */
    static int merge(Subset **subsets, int x, int y);

};

#endif // GRAPH_H
