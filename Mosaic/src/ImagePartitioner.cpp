#include "src/ImagePartitioner.h"
#include "src/ImageSetBuilder.h"

#include <cmath>
#include <ctime>

ImagePartitioner::ImagePartitioner(QImage *image, QObject *parent)
    : QObject(parent),
      m_image(image),
      m_scale_const_value(-1),
      m_min_pixel_count(-1),
      m_grid(0),
      m_tolerance((double)ComplexGrid::DEFAULT_TOLERANCE/100),
      m_destination("")
{
}

ImagePartitioner::~ImagePartitioner()
{
    if(m_image)
        delete m_image;
}

QImage *ImagePartitioner::image() const
{
    return m_image;
}

void ImagePartitioner::setImage(QImage *image)
{
    m_image = image;
}

int ImagePartitioner::f(std::pair<int, int> e)
{
    return e.second * m_image->width() + e.first;
}

int ImagePartitioner::f(int x, int y)
{
    return f(std::pair<int, int>(x, y));
}

std::pair<int, int> ImagePartitioner::f_i(int i)
{
    int y = std::floor((double)i / m_image->width());
    int x = i % m_image->width();

    return std::pair<int, int>(x, y);
}

double ImagePartitioner::wf(int x, int y)
{
    /* Convert to coords */
    std::pair<int, int> a = f_i(x);
    std::pair<int, int> b = f_i(y);

    /* Calculate distance */
    return calculateEuclideanDist(
        m_image->pixel(a.first, a.second),
        m_image->pixel(b.first, b.second)
    );
}

double ImagePartitioner::calculateEuclideanDist(unsigned c1, unsigned c2)
{
    return std::sqrt(std::pow(qRed(c1) - qRed(c2), 2) +
                     std::pow(qGreen(c1) - qGreen(c2), 2) + std::pow(qBlue(c1) - qBlue(c2), 2));
}

double ImagePartitioner::MInt(Subset *a, Subset *b)
{
    return std::min(a->Int + t(a), b->Int + t(b));
}

double ImagePartitioner::t(Subset *c)
{
    return scaleConstValue() / c->count;
}

QString ImagePartitioner::destination() const
{
    return m_destination;
}

void ImagePartitioner::setDestination(const QString &destination)
{
    m_destination = destination;
}

double ImagePartitioner::tolerance() const
{
    return m_tolerance;
}

void ImagePartitioner::setTolerance(double tolerance)
{
    m_tolerance = tolerance;
}

ComplexGrid *ImagePartitioner::grid() const
{
    return m_grid;
}

void ImagePartitioner::setGrid(ComplexGrid *grid)
{
    m_grid = grid;
}

int ImagePartitioner::minPixelCount() const
{
    return (m_min_pixel_count == -1 ? SIZE_THRESHOLD : m_min_pixel_count);
}

void ImagePartitioner::setMinPixelCount(int count)
{
    m_min_pixel_count = count;
}

int ImagePartitioner::scaleConstValue() const
{
    return (m_scale_const_value == -1 ? SCALE_CONSTANT : m_scale_const_value);
}

void ImagePartitioner::setScaleConstValue(int value)
{
    m_scale_const_value = value;
}

void ImagePartitioner::run()
{
    qDebug() << "ImagePartitioner::run(): Scale constant is " << scaleConstValue();
    qDebug() << "ImagePartitioner::run(): Size threshold " << minPixelCount();
    qDebug() << "ImagePartitioner::run(): Width " << m_image->width();
    qDebug() << "ImagePartitioner::run(): Height " << m_image->height();

    qDebug() << "ImagePartitioner::run(): Adding edges...";

    emit started(m_image->width() * m_image->height());
    emit statusChanged("Identifying edges...");

    /* Graph for pixels to be partitioned */
    Graph graph(m_image->width() * m_image->height());

    /* Create edges */
    int k = 0;
    for (int x = 0; x < m_image->width(); x++)
    {
        for (int y = 0; y < m_image->height(); y++)
        {
            int c = f(x, y);

            /* This pixel has neighbour in s (South) */
            if(y < m_image->height() - 1)
            {
                int s = f(x, y + 1);

                /* Add edge */
                graph.insert(Graph::Edge(wf(c, s), c, s));
            }

            /* This pixel has neighbour in e (East) */
            if(x < m_image->width() - 1)
            {
                int e = f(x + 1, y);

                /* Add edge */
                graph.insert(Graph::Edge(wf(c, e), c, e));
            }

            emit progressValueChanged(++k);
        }
    }

    emit statusChanged("Sorting...");

    qDebug() << "ImagePartitioner::run(): Sorting edges...";

    /* Sort all the edges in non-decreasing order of their weight */
    std::sort(graph.edges().begin(), graph.edges().end());

    /*
    for(int i = 0; i < graph.edges().size(); i++)
        qDebug() << "ImagePartitioner::run(): "
                 << QString("Weight #%1 is %2").arg(i).arg(graph.edges()[i].weight);
    */

    qDebug() << "ImagePartitioner::run(): Creating subsets... (" << graph.getSize() << ")";

    /* Create |V| subsets */
    Subset** subsets =  new Subset*[graph.getSize()];
    for(uint i = 0; i < graph.getSize(); i++)
        subsets[i] = new Subset(0, i, 0, i);

    qDebug() << "ImagePartitioner::run(): Traversing edges...";

    emit started(graph.edges().size());
    emit statusChanged("Processing edges...");

    /* For each edge */
    k = 0;
    for(std::vector<Graph::Edge>::iterator it = graph.edges().begin();
        it != graph.edges().end(); it++)
    {
        Graph::Edge e = *it;

        /*
        qDebug() << "ImagePartitioner::run(): Inspecting edge with weight " << e.weight;
        qDebug() << "ImagePartitioner::run(): a " << e.a;
        qDebug() << "ImagePartitioner::run(): b " << e.b;
        */

        /* If a & b are in disjoint components */
        int x = Subset::find(subsets, e.a);
        int y = Subset::find(subsets, e.b);

        if(x != y)
        {
            /* If e.weight is small compared to the internal difference of both
               these components, then merge these two components. */
            if(e.weight < MInt(subsets[x], subsets[y]))
            {
                int root = Subset::merge(subsets, x, y);
                subsets[root]->Int = e.weight;
            }
        }

        emit progressValueChanged(++k);
    }

    /* Process too small regions. Iterate over all edges and merge disjoint regions that have
       size < threshold (either one). */
    emit started(graph.edges().size());
    emit statusChanged("Correcting...");

    /* For each edge */
    k = 0;
    for(std::vector<Graph::Edge>::iterator it = graph.edges().begin();
        it != graph.edges().end(); it++)
    {
        Graph::Edge e = *it;

        /* If a & b are in disjoint components */
        int x = Subset::find(subsets, e.a);
        int y = Subset::find(subsets, e.b);

        if(x != y)
        {
            if(subsets[x]->count < minPixelCount() || subsets[y]->count < minPixelCount())
            {
                int root = Subset::merge(subsets, x, y);
                subsets[root]->Int = e.weight;
            }
        }

        emit progressValueChanged(++k);
    }

    /* Great! Let ComplexGrid take over from here. */
    emit started(graph.getSize());
    emit statusChanged("Drawing grid...");

    m_grid->setSubsets(subsets);
    m_grid->setTolerance(m_tolerance);
    m_grid->createGrid();

    /* Visualize */
    if(!m_destination.isEmpty())
    {
        std::srand(std::time(NULL));
        auto randColor = []()
        {
            int r = ((std::rand() % 256) + 0.618033988749895f * (std::rand() % 400)) / 2;
            int g = ((std::rand() % 256) + 0.618033988749895f * (std::rand() % 400)) / 2;
            int b = ((std::rand() % 256) + 0.618033988749895f * (std::rand() % 400)) / 2;

            return qRgb(r, g, b);
        };

        std::map<int, QRgb> colormap;

        k = 0;
        for (int x = 0; x < m_image->width(); x++)
        {
            for (int y = 0; y < m_image->height(); y++)
            {
                QRgb color;
                int parent = Subset::find(subsets, f(x, y));

                /*
                qDebug() << "ImagePartitioner::run(): "
                         << QString("f(%1, %2) translates to %3.").arg(x).arg(y).arg(f(x, y));
                qDebug() << "ImagePartitioner::run(): Parent is " << parent;
                */

                std::map<int, QRgb>::iterator it = colormap.find(parent);
                if(it == colormap.end())
                {
                    color = randColor();
                    colormap[parent] = color;
                }
                else
                {
                    color = (*it).second;
                }

                m_image->setPixel(x, y, color);

                emit progressValueChanged(++k);
            }
        }

        QFileInfo destinationInfo(m_destination);
        QString segmentedImage = destinationInfo.absoluteDir().path() + "/" +
                destinationInfo.baseName() + ".seg." + destinationInfo.completeSuffix();

        qDebug() << "ImagePartitioner::run(): Number of segments: " << colormap.size();
        qDebug() << "ImagePartitioner::run(): Saving segmented image: " << segmentedImage;

        m_image->save(segmentedImage);
    }

    qDebug() << "ImagePartitioner::run(): Disposing subsets...";

    /* Clean up */
    for(int i = 0; i < m_image->width() * m_image->height(); i++)
        delete subsets[i];
    delete [] subsets;

    qDebug() << "ImagePartitioner::run(): Segmentation finished:";

    emit finished();
}

void ImagePartitioner::reset()
{
    qDebug() << "ImagePartitioner::reset()";

    m_tolerance = (double)ComplexGrid::DEFAULT_TOLERANCE/100;
    m_destination = "";

    if(m_image)
    {
        delete m_image;
        m_image = 0;
    }

    if(m_grid)
    {
        m_grid = 0;
    }
}
