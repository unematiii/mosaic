#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

namespace Ui {
class AboutDialog;
}

class QAbstractButton;

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

public slots:
    void buttonBoxItemClick(QAbstractButton* button);

private:
    Ui::AboutDialog *ui;
};

#endif // ABOUTDIALOG_H
