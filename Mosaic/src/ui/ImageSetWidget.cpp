#include <QFileDialog>
#include <QDebug>

#include "src/ImageSetBuilder.h"
#include "src/ui/ImageSetWidget.h"
#include "src/ui/ProgessDialog.h"

ImageSetWidget::ImageSetWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    initializeComponents();
}

void ImageSetWidget::initializeComponents()
{
    /* Disable save button */
    saveImageSetBtn->setEnabled(false);

    /* Transparent label */
    sizeValue->setStyleSheet("background-color: rgba(255, 255, 255, 10);");

    /* Grid dimensions */
    sizeSlider->setValue(64);

    /* Progress dialog */
    m_progressDialog = new ProgressDialog(this);

    /* Builder */
    m_builder = new ImageSetBuilder(this);

    /* Signals & slots */
    connect(imagesFolderBtn, SIGNAL(clicked()), this,
            SLOT(selectFolderPath()));
    connect(imagesFolderPath, SIGNAL(textChanged(QString)),
            this, SLOT(imagesFolderPathChanged()));

    connect(outputFolderBtn, SIGNAL(clicked()), this,
            SLOT(selectOutputFolder()));
    connect(outputFolderPath, SIGNAL(textChanged(QString)),
            this, SLOT(imagesFolderPathChanged()));

    connect(sizeSlider, SIGNAL(valueChanged(int)),
            this, SLOT(sizeValueChanged(int)));
    connect(resizeImages, SIGNAL(toggled(bool)),
            this, SLOT(setResizeModeOn(bool)));

    connect(saveImageSetBtn, SIGNAL(clicked()),
            this, SLOT(selectSavePath()));

    connect(m_builder, SIGNAL(started(int)),
            m_progressDialog, SLOT(start(int)));
    connect(m_builder, SIGNAL(progressValueChanged(int)),
            m_progressDialog, SLOT(setProgress(int)));
    connect(m_builder, SIGNAL(finished()),
            m_progressDialog, SLOT(close()));
    connect(m_builder, SIGNAL(finished()),
            m_progressDialog, SLOT(resetFields()));
    connect(m_builder, SIGNAL(finished()),
            this, SLOT(imagesFolderPathChanged()));
}


void ImageSetWidget::imagesFolderPathChanged()
{
    if(!imagesFolderPath->text().length() || (!outputFolderPath->text().length() && resizeImages->isChecked()))
        saveImageSetBtn->setEnabled(false);
    else
        saveImageSetBtn->setEnabled(true);
}

void ImageSetWidget::setResizeModeOn(bool value)
{
    imagesFolderPathChanged();

    qDebug() << "ImageSetWidget::setResizeModeOn(bool): On: " << value;
}

void ImageSetWidget::sizeValueChanged(int value)
{
    sizeValue->setText(QString("%1 x %2").arg(value).arg(value));
}

void ImageSetWidget::resetFields()
{
    /* Grid dimensions */
    sizeSlider->setValue(32);
    sizeValue->setText(QString("%1 x %2").arg(32).arg(32));

    /* Enable / disable crop */
    resizeImages->setChecked(true);
    useCustomSizeBox->setChecked(false);

    /* Paths */
    imagesFolderPath->setText("");
    outputFolderPath->setText("");
}

void ImageSetWidget::selectFolderPath()
{
    QString fileName = QFileDialog::getExistingDirectory(this);

    if(!fileName.isEmpty())
        imagesFolderPath->setText(fileName);

    qDebug() << "ImageSetWidget::selectFolderPath() : Directory " << fileName;
}

void ImageSetWidget::selectSavePath()
{
    QString fileName = QFileDialog::getSaveFileName(this, "", "", "*.images");
    if(!fileName.isEmpty())
    {
        if(!fileName.endsWith(".images", Qt::CaseInsensitive))
            fileName.append(".images");

        qDebug() << "ImageSetWidget::selectSavePath() : File " << fileName;

        /* Disable save button */
        saveImageSetBtn->setEnabled(false);

        /* Build image set */
        createImageSet(fileName);
    }
}

void ImageSetWidget::selectOutputFolder()
{
    QString fileName = QFileDialog::getExistingDirectory(this);

    if(!fileName.isEmpty())
        outputFolderPath->setText(fileName);

    qDebug() << "ImageSetWidget::selectOutputFolder() : Directory " << fileName;
}

void ImageSetWidget::createImageSet(const QString &filename)
{
    m_builder->setImagesDirectory(imagesFolderPath->text());
    m_builder->setFileName(filename);
    m_builder->setOutputDirectory(outputFolderPath->text());
    if(useCustomSizeBox->isChecked())
        m_builder->setCropSize(sizeSlider->value());
    m_builder->setCrop(resizeImages->isChecked());
    m_builder->build();
}

ImageSetWidget::~ImageSetWidget()
{
}
