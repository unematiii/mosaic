#include <QDebug>
#include <QDesktopWidget>

#include "ProgessDialog.h"
#include "ui_ProgessDialog.h"

ProgressDialog::ProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgessDialog),
    m_max()
{
    ui->setupUi(this);

    setWindowFlags(((windowFlags() | Qt::CustomizeWindowHint)
                           & ~Qt::WindowCloseButtonHint));
}

ProgressDialog::~ProgressDialog()
{
    delete ui;
}

void ProgressDialog::center()
{
    /* qDebug() << "ProgressDialog::center()"; */

    QRect desktopRect = QApplication::desktop()->availableGeometry(this);
    QPoint center = desktopRect.center();
    move(center.x() - width() * 0.5, center.y() - height() * 0.5);
}

void ProgressDialog::start(int max)
{
    /* qDebug() << "ProgressDialog::start(): Max is " << max; */
    m_max = max;
    ui->progressBar->setValue(0);

    center();
    show();
}

void ProgressDialog::setProgress(int value)
{
    /* qDebug() << "ProgressDialog::setProgress(): Value is "
             << value; */

    int ratio = value / (float)m_max * 100;
    ui->progressBar->setValue(ratio);
}

void ProgressDialog::resetFields()
{
    m_max = 0;
    ui->progressBar->setValue(0);
    setWindowTitle("Working...");
}

void ProgressDialog::finish()
{
    /* qDebug() << "ProgressDialog::finish()"; */

    resetFields();
    hide();
}

void ProgressDialog::statusChanged(const QString &status)
{
    setWindowTitle(status);
}
