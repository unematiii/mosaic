#ifndef PROGESSDIALOG_H
#define PROGESSDIALOG_H

#include <QDialog>

namespace Ui {
class ProgessDialog;
}

class ProgressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProgressDialog(QWidget *parent = 0);
    ~ProgressDialog();

public slots:
    void center();
    void start(int max);
    void setProgress(int value);
    void resetFields();
    void finish();
    void statusChanged(const QString&status);

private:
    Ui::ProgessDialog *ui;

    int m_max;

};

#endif // PROGESSDIALOG_H
