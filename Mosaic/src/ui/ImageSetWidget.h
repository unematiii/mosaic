#ifndef IMAGESETWIDGET_H
#define IMAGESETWIDGET_H

#include <QWidget>
#include "ui_ImageSetWidget.h"

class ProgressDialog;
class ImageSetBuilder;

class ImageSetWidget : public QWidget, protected Ui::ImageSetWidget
{
    Q_OBJECT

public:
    explicit ImageSetWidget(QWidget *parent = 0);
    ~ImageSetWidget();

public slots:
    void imagesFolderPathChanged();
    void setResizeModeOn(bool value);
    void sizeValueChanged(int value);
    void selectFolderPath();
    void selectSavePath();
    void selectOutputFolder();
    void createImageSet(const QString& filename);
    void resetFields();

private:
    void initializeComponents();

    ProgressDialog *m_progressDialog;
    ImageSetBuilder *m_builder;

};

#endif // IMAGESETWIDGET_H
