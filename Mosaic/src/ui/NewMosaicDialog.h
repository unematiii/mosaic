#ifndef NEWMOSAICDIALOG_H
#define NEWMOSAICDIALOG_H

#include <QDialog>

#include <QtConcurrent/QtConcurrent>

class QAbstractButton;
class ImageSetWidget;
class ImagePartitioner;
class ProgressDialog;
class MosaicPainter;

namespace Ui {
    class NewMosaicDialog;
}

class NewMosaicDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewMosaicDialog(QWidget *parent = 0);
    ~NewMosaicDialog();

public slots:
    void buttonBoxItemClick(QAbstractButton* button);
    void selectSourceImage();
    void selectImageSet();
    void filePathChanged();
    void resetFields();
    void center();
    void buildMosaic(const QString &target);

    /* This will be called when ImagePartitioner has been finished! */
    void paintMosaic();

    void saveMosaicBtnClicked();
    void showAboutDialog();
    void validateSizeBoxes();

private:
    Ui::NewMosaicDialog *ui;
    ImageSetWidget *m_imageSetWidget;
    ProgressDialog *m_progressDialog;
    MosaicPainter *m_mosaicPainter;
    QFutureWatcher<void> *m_future_watcher;
    ImagePartitioner *m_partitioner;

    void initializeComponents();

};

#endif // NEWMOSAICDIALOG_H
