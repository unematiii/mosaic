#include <QDesktopWidget>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QVBoxLayout>

#include "src/ui/AboutDialog.h"
#include "src/ui/ImageSetWidget.h"
#include "src/ui/NewMosaicDialog.h"
#include "src/ui/ProgessDialog.h"

#include "src/MosaicPainter.h"
#include "src/ImagePartitioner.h"

#include "src/misc/Grid.h"

#include "ui_NewMosaicDialog.h"

NewMosaicDialog::NewMosaicDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewMosaicDialog),
    m_mosaicPainter(0),
    m_future_watcher(new QFutureWatcher<void>()),
    m_partitioner(new ImagePartitioner())
{
    ui->setupUi(this);
    initializeComponents();
}

NewMosaicDialog::~NewMosaicDialog()
{
    if(m_future_watcher)
        delete m_future_watcher;

    if(m_partitioner)
        delete m_partitioner;

    delete ui;
}

void NewMosaicDialog::center()
{
    qDebug() << "NewMosaicDialog::center()";

    QRect desktopRect = QApplication::desktop()->availableGeometry(this);
    QPoint center = desktopRect.center();
    move(center.x() - width() * 0.5, center.y() - height() * 0.5);
}

void NewMosaicDialog::initializeComponents()
{
    /* Title */
    setWindowTitle("Mosaic Painter");

    /* Show minimize button */
    setWindowFlags(Qt::WindowMinimizeButtonHint);

    /* Center */
    center();

    /* ImageSetWidget */
    m_imageSetWidget = new ImageSetWidget(ui->createImageSetTab);
    QVBoxLayout* layout = new QVBoxLayout();
    layout->setSizeConstraint(QLayout::SetMinimumSize);
    layout->addWidget(m_imageSetWidget);
    layout->setMargin(0);
    ui->createImageSetTab->setLayout(layout);

    /* Progress dialog */
    m_progressDialog = new ProgressDialog(this);

    /* Mosaic builder */
    m_mosaicPainter = new MosaicPainter(this);

    /* Disable save button */
    ui->saveMosaicBtn->setEnabled(false);

    /* Minimum & maximum tile size */
    QStringList sizes;
    sizes << "8" << "16" << "32" << "64";

    ui->minimumTileSize->addItems(sizes);
    ui->maximumTileSize->addItems(sizes);

    /* About dialog */
    QPushButton* clickedButton = ui->buttonBox->button(QDialogButtonBox::Help);
    clickedButton->setText("About");

    /* Signals & slots */
    connect(ui->buttonBox, SIGNAL(clicked(QAbstractButton*)),
            this, SLOT(buttonBoxItemClick(QAbstractButton*)));

    connect(ui->saveMosaicBtn, SIGNAL(clicked()),
            this, SLOT(saveMosaicBtnClicked()));

    connect(ui->srcImageBtn, SIGNAL(clicked()), this,
            SLOT(selectSourceImage()));
    connect(ui->imageSetBtn, SIGNAL(clicked()), this,
            SLOT(selectImageSet()));
    connect(ui->srcImagePath, SIGNAL(textChanged(QString)),
            this, SLOT(filePathChanged()));
    connect(ui->imageSetPath, SIGNAL(textChanged(QString)),
            this, SLOT(filePathChanged()));

    connect(ui->minimumTileSize, SIGNAL(currentIndexChanged(int)),
            this, SLOT(validateSizeBoxes()));
    connect(ui->maximumTileSize, SIGNAL(currentIndexChanged(int)),
            this, SLOT(validateSizeBoxes()));

    connect(m_mosaicPainter, SIGNAL(started(int)),
            m_progressDialog, SLOT(start(int)));
    connect(m_mosaicPainter, SIGNAL(progressValueChanged(int)),
            m_progressDialog, SLOT(setProgress(int)));
    connect(m_mosaicPainter, SIGNAL(finished()),
            m_progressDialog, SLOT(finish()));
    connect(m_mosaicPainter, SIGNAL(finished()),
            m_progressDialog, SLOT(resetFields()));
    connect(m_mosaicPainter, SIGNAL(finished()),
            m_mosaicPainter, SLOT(resetFields()));
    connect(m_mosaicPainter, SIGNAL(statusChanged(QString)),
            m_progressDialog, SLOT(statusChanged(QString)));

    connect(m_future_watcher, SIGNAL(finished()),
            this, SLOT(paintMosaic()));

    connect(m_partitioner, SIGNAL(started(int)),
            m_progressDialog, SLOT(start(int)));
    connect(m_partitioner, SIGNAL(progressValueChanged(int)),
            m_progressDialog, SLOT(setProgress(int)));
    connect(m_partitioner, SIGNAL(finished()),
            m_progressDialog, SLOT(finish()));
    connect(m_partitioner, SIGNAL(statusChanged(QString)),
            m_progressDialog, SLOT(statusChanged(QString)));

    /* Set controls to their defaults */
    resetFields();
}

void NewMosaicDialog::resetFields()
{
    qDebug() << "NewMosaicDialog::resetFields()";

    /* Text fields */
    ui->srcImagePath->setText("");
    ui->imageSetPath->setText("");

    /* Grid dimensions */
    ui->minimumTileSize->setCurrentIndex(0);
    ui->maximumTileSize->setCurrentIndex(3);

    /* Imageset Widger */
    m_imageSetWidget->resetFields();

    ui->scaleConstValue->setValue(ImagePartitioner::SCALE_CONSTANT);
    ui->minimumRegionValue->setValue(ImagePartitioner::SIZE_THRESHOLD);
    ui->toleranceValue->setValue(ComplexGrid::DEFAULT_TOLERANCE);
    ui->outputSegmentedImage->setChecked(false);

    /* Painter */
    if(m_mosaicPainter)
        m_mosaicPainter->resetFields();

    /* Partitioner */
    if(m_partitioner)
        m_partitioner->reset();
}

void NewMosaicDialog::buttonBoxItemClick(QAbstractButton* button)
{
    qDebug() << "NewMosaicDialog::buttonBoxItemClick(QAbstractButton* button)";

    QPushButton* clickedButton = (QPushButton*)button;

    /* Close */
    if(clickedButton == ui->buttonBox->button(QDialogButtonBox::Close))
        close();

    /* Reset fields */
    if(clickedButton == ui->buttonBox->button(QDialogButtonBox::RestoreDefaults))
        resetFields();

    /* About */
    if(clickedButton == ui->buttonBox->button(QDialogButtonBox::Help))
        showAboutDialog();
}

void NewMosaicDialog::buildMosaic(const QString& target)
{
    /* Painter */
    m_mosaicPainter->setImageSetFile(ui->imageSetPath->text());
    m_mosaicPainter->setSourceImageFile(ui->srcImagePath->text());
    m_mosaicPainter->setTargetImageFile(target);

    int cols, rows;
    cols = rows = 0;
    QImage* painter_image = new QImage(ui->srcImagePath->text());

    unsigned min_tile_size =
            ui->minimumTileSize->currentText().toUInt();
    unsigned max_tile_size =
            ui->maximumTileSize->currentText().toUInt();
    bool is_simple_grid = false;

    qDebug() << "NewMosaicDialog::buildMosaic():"
             << QString("%1 : %2").arg(min_tile_size).arg(max_tile_size) ;

    /* Use simple grid */
    if(min_tile_size == max_tile_size)
    {

        cols = std::floor(painter_image->size().width() / max_tile_size);
        rows = std::floor(painter_image->size().height() / max_tile_size);

        m_mosaicPainter->setGrid(
                    new SimpleGrid(cols * max_tile_size,
                                   rows * max_tile_size, max_tile_size)
        );

        *painter_image = painter_image->scaledToWidth(cols * max_tile_size);

        *painter_image = painter_image->scaled(
                    QSize(cols * max_tile_size,
                          rows * max_tile_size)
        );

        is_simple_grid = true;
    }
    /* Use complex grid */
    else
    {
        cols = std::floor(painter_image->size().width() / max_tile_size);
        rows = std::floor(painter_image->size().height() / max_tile_size);

        ComplexGrid* grid = new ComplexGrid(cols * max_tile_size,
                                            rows * max_tile_size, min_tile_size, max_tile_size);

        m_mosaicPainter->setGrid(grid);
        m_partitioner->setGrid(grid);

        *painter_image = painter_image->scaledToWidth(cols * max_tile_size);
        *painter_image = painter_image->scaled(QSize(cols * max_tile_size,
                                             rows * max_tile_size));

        qDebug() << "NewMosaicDialog::buildMosaic(): Cropped image dimensions: "
                 << painter_image->width() << "x" << painter_image->height();
    }

    /* Set cropped image */
    m_mosaicPainter->setMosaic(painter_image);

    qDebug() << "NewMosaicDialog::buildMosaic(): Grid type is: "
             << (is_simple_grid ? "SIMPLE" : "COMPLEX");

    qDebug() << "NewMosaicDialog::buildMosaic(): Cols x Rows: "
             << cols << "" << rows;

    /* Partition image */
    if(!is_simple_grid)
    {
        qDebug() << "NewMosaicDialog::buildMosaic(): Image " << target;

        m_partitioner->setImage(new QImage(*painter_image));
        m_partitioner->setMinPixelCount(ui->minimumRegionValue->value());
        m_partitioner->setScaleConstValue(ui->scaleConstValue->value());
        m_partitioner->setTolerance((double)ui->toleranceValue->value() / 100);

        if(ui->outputSegmentedImage->isChecked())
            m_partitioner->setDestination(target);
        else
            m_partitioner->setDestination("");

        QFuture<void> future = QtConcurrent::run(m_partitioner, &ImagePartitioner::run);
        m_future_watcher->setFuture(future);
    }
    else
    {
        paintMosaic();
    }
}

void NewMosaicDialog::paintMosaic()
{
    m_mosaicPainter->paint();
}

void NewMosaicDialog::saveMosaicBtnClicked()
{
    QString fileName = QFileDialog::getSaveFileName(
        this, "", "", "JPG (*.jpg);;JPEG (*.jpeg);;PNG (*.png);;GIF (*.gif);;SVG (*.svg);;TIFF (*.tiff)"
    );
    if(!fileName.isEmpty())
        buildMosaic(fileName);
}

void NewMosaicDialog::showAboutDialog()
{
    AboutDialog* aboutDlg = new AboutDialog(this);
    aboutDlg->show();
}

void NewMosaicDialog::validateSizeBoxes()
{
    qDebug() << "NewMosaicDialog::validateSizeBoxes()";

    if(ui->minimumTileSize->currentText().toUInt()
            > ui->maximumTileSize->currentText().toUInt())
        ui->minimumTileSize->setCurrentIndex(ui->maximumTileSize->currentIndex());

    if(ui->minimumTileSize->currentText().toUInt()
            == ui->maximumTileSize->currentText().toUInt())
        ui->advancedGridGroup->setEnabled(false);
    else
        ui->advancedGridGroup->setEnabled(true);
}

void NewMosaicDialog::selectSourceImage()
{
    QString fileName = QFileDialog::getOpenFileName(
        this, "", "", "JPG (*.jpg);;JPEG (*.jpeg);;PNG (*.png);;GIF (*.gif);;TIFF (*.tiff);;All Files (*.*)"
    );
    if(!fileName.isEmpty())
        if(QImage(fileName).depth() == 32)
            ui->srcImagePath->setText(fileName);
        else
            QMessageBox::critical(this, "Incompatible image depth",
                "Sorry, but Mosaic Painter only supports 32-bit images.",
                QMessageBox::Ok, QMessageBox::Ok);

    qDebug() << "MainWindow::selectSourceImage() : File " << fileName;
}

void NewMosaicDialog::selectImageSet()
{
    QString fileName = QFileDialog::getOpenFileName(this, "", "", "Image set (*.images)");
    if(!fileName.isEmpty())
    {
        ui->imageSetPath->setText(fileName);
    }

    qDebug() << "MainWindow::selectImageSet() : File " << fileName;
}

void NewMosaicDialog::filePathChanged()
{
     if(!ui->srcImagePath->text().length() || !ui->imageSetPath->text().length())
         ui->saveMosaicBtn->setEnabled(false);
     else
         ui->saveMosaicBtn->setEnabled(true);
}
