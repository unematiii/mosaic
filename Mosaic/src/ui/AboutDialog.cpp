#include "AboutDialog.h"
#include "ui_AboutDialog.h"

#include <QDebug>
#include <QDesktopWidget>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    connect(ui->buttonBox, SIGNAL(clicked(QAbstractButton*)),
            this, SLOT(buttonBoxItemClick(QAbstractButton*)));

    QRect desktopRect = QApplication::desktop()->availableGeometry(this);
    QPoint center = desktopRect.center();
    move(center.x() - width() * 0.5, center.y() - height() * 0.5);
}

void AboutDialog::buttonBoxItemClick(QAbstractButton* button)
{
    qDebug() << "AboutDialog::buttonBoxItemClick(QAbstractButton* button)";

    QPushButton* clickedButton = (QPushButton*)button;

    /* Close */
    if(clickedButton == ui->buttonBox->button(QDialogButtonBox::Close))
        close();
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
