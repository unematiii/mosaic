#include <QDebug>
#include <QDir>
#include <QImage>
#include <QPixmap>
#include <QStringList>
#include <QXmlStreamWriter>

#include <functional>

#include "src/ImageSetBuilder.h"

ImageSetBuilder::ImageSetBuilder(QObject *parent)
    : QObject(parent),
      m_crop_size(-1),
      m_crop(true),
      m_future_watcher(new QFutureWatcher<void>()),
      m_xml_writer(0),
      m_xml_file(0)
{
    /* Set up future watcher */
    connect(m_future_watcher, SIGNAL(progressValueChanged(int)),
            this, SLOT(emitProgressValueChanged(int)));
    connect(m_future_watcher, SIGNAL(started()),
            this, SLOT(emitStarted()));
    connect(m_future_watcher, SIGNAL(finished()),
            this, SLOT(emitFinished()));
}

ImageSetBuilder::~ImageSetBuilder()
{
    delete m_future_watcher;

    if(m_xml_file)
        delete m_xml_file;
    if(m_xml_writer)
        delete m_xml_writer;
}

void ImageSetBuilder::setFileName(const QString& filename)
{
    this->m_filename = filename;
}

void ImageSetBuilder::calculateMetrics(QImage image, QString newsrc)
{
    unsigned color = ImageSetBuilder::calculateAvgColor(image);

    /* Write to file (blocking) */
    m_mutex.lock();
    m_xml_writer->writeStartElement("image");
    m_xml_writer->writeTextElement("path", newsrc);
    m_xml_writer->writeTextElement("size", QString("%1").arg(image.width()));
    m_xml_writer->writeTextElement("color", QString("%1").arg(color));
    m_xml_writer->writeEndElement();
    m_mutex.unlock();
}

void ImageSetBuilder::processImage(QString src)
{
    QImage image;
    if(image.load(src))
    {
        QString newsrc = src;

        /* Crop */
        if(m_crop)
        {
            if(m_crop_size > 0)
            {
                image = image.scaledToWidth(m_crop_size);
                image = image.scaled(m_crop_size, m_crop_size);

                QFileInfo info(src);
                newsrc = m_output_directory + "/" +
                        QString("img_%1_%1_").arg(m_crop_size) + info.fileName();


                qDebug() << "ImageSetBuilder::processImage(): Saving cropped image as  "
                         << newsrc << "...";

                image.save(newsrc);
                calculateMetrics(image, newsrc);
            }
            else
            {
                unsigned sizes[] = {0x40, 0x20, 0x10, 0x8};

                for(unsigned i = 0; i < (sizeof(sizes) / sizeof(unsigned)); i++)
                {
                    image = image.scaledToWidth(sizes[i]);
                    image = image.scaled(sizes[i], sizes[i]);

                    QFileInfo info(src);
                    newsrc = m_output_directory + "/" +
                            QString("img_%1_%1_").arg(sizes[i]) + info.fileName();


                    qDebug() << "ImageSetBuilder::processImage(): Saving cropped image as  "
                             << newsrc << "...";

                    image.save(newsrc);
                    calculateMetrics(image, newsrc);
                }
            }

        } else {
            calculateMetrics(image, newsrc);
        }
    }
    else
    {
        qDebug() << "ImageSetBuilder::processImage(): Error  "
                 << QString("Failed to open image %1.").arg(src);

        emit error(QString("Failed to open image %1.").arg(src));
        return;
    }
}

unsigned ImageSetBuilder::calculateAvgColor(const QImage &image)
{
    int r, g, b;
    r = g = b = 0;
    int count = image.height() * image.width();

    for(int x = 0; x < image.width(); x++)
    {
            for(int y = 0; y < image.height(); y++)
            {
                QColor color(image.pixel(x,y));
                r += color.red();
                g += color.green();
                b += color.blue();
            }
    }

    r /= count;
    g /= count;
    b /= count;

    return qRgb(r, g, b);
}

void ImageSetBuilder::emitProgressValueChanged(int value)
{
    emit progressValueChanged(value);
}

void ImageSetBuilder::emitStarted()
{
    emit started(m_files_list.size());
}

void ImageSetBuilder::resetFields()
{
    qDebug() << "ImageSetBuilder::resetFields()";

    m_filename = "";
    m_crop_size = -1;
    m_crop = true;
    m_output_directory = "";
    m_files_list.clear();
    m_images_directory = "";
    m_xml_file = 0;
    m_xml_writer = 0;
}

void ImageSetBuilder::emitFinished()
{
    /* Finalize */
    m_xml_writer->writeEndElement();
    m_xml_writer->writeEndDocument();
    m_xml_file->close();

    delete m_xml_writer;
    delete m_xml_file;

    /* Reset */
    resetFields();

    emit finished();
}

void ImageSetBuilder::build()
{
    QDir dir(m_images_directory);
    QStringList filters;
    filters
            << "*.jpg"
            << "*.jpeg"
            << "*.png"
            << "*.tiff"
            << "*.gif";

    QFileInfoList images = dir.entryInfoList(filters, QDir::Files);
    for(QFileInfo info : images)
        m_files_list.append(info.filePath());

    qDebug() << "ImageSetBuilder::build(): # files to process: "
           << m_files_list.size();

    m_xml_writer = new QXmlStreamWriter();
    m_xml_file = new QFile(m_filename);
    m_xml_file->open(QFile::WriteOnly);
    m_xml_writer->setDevice(m_xml_file);
    m_xml_writer->writeStartDocument();
    m_xml_writer->writeStartElement("images");

    QFuture<void> future = QtConcurrent::map(
                m_files_list, std::bind1st(std::mem_fun(&ImageSetBuilder::processImage), this)
    );
    m_future_watcher->setFuture(future);
}
