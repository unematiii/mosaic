#ifndef IMAGEPARTITIONER_H
#define IMAGEPARTITIONER_H

#include <QImage>
#include <QList>
#include <QRect>

#include "src/misc/Grid.h"

class ImagePartitioner : public QObject
{
    Q_OBJECT

public:
    static const int SCALE_CONSTANT = 10000;
    static const int SIZE_THRESHOLD = 16;

    ImagePartitioner(QImage *image = 0, QObject* parent = 0);
    virtual ~ImagePartitioner();

    QImage *image() const;
    void setImage(QImage *image);

    double calculateEuclideanDist(unsigned c1, unsigned c2);

    int scaleConstValue() const;
    void setScaleConstValue(int value);

    int minPixelCount() const;
    void setMinPixelCount(int count);

    ComplexGrid *grid() const;
    void setGrid(ComplexGrid *grid);

    double tolerance() const;
    void setTolerance(double tolerance);

    QString destination() const;
    void setDestination(const QString &destination);

public slots:
    void run();
    void reset();

signals:
    void progressValueChanged(int);
    void started(int);
    void finished();
    void statusChanged(const QString&);

protected:
    std::pair<int, int> f(int i);
    int f(std::pair<int, int> e);
    int f(int x, int y);
    std::pair<int, int> f_i(int i);

    double wf(int x, int y);
    double MInt(Subset *a, Subset *b);
    double t(Subset *c);

private:
    QImage *m_image;
    int m_scale_const_value;
    int m_min_pixel_count;
    ComplexGrid *m_grid;
    double m_tolerance;
    QString m_destination;

};

#endif // IMAGEPARTITIONER_H
