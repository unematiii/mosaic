#ifndef MOSAICPAINTER_H
#define MOSAICPAINTER_H

#include <QObject>
#include <QList>
#include <QRect>

#include <QtConcurrent/QtConcurrent>

#include "src/ImageSetItem.h"
#include "src/misc/kdtree.h"

class QImage;
class Grid;

namespace bg = boost::geometry;
namespace bgm = bg::model;
namespace si = spatial_index;

typedef bgm::point<int, 3, bg::cs::cartesian> Point;
typedef std::map<unsigned, si::kdtree<ImageSetItem, Point>> TreeMap;

class MosaicPainter : public QObject
{
    Q_OBJECT

public:
    enum GRID_TYPE { SIMPLE, COMPLEX };

    MosaicPainter(QObject* parent = 0);
    ~MosaicPainter();

    QString getImageSetFile() const;
    void setImageSetFile(const QString &file);

    QString getSourceImageFile() const;
    void setSourceImageFile(const QString &file);

    QString getTargetImageFile() const;
    void setTargetImageFile(const QString &file);

    ImageSetItem parseImage(QXmlStreamReader &xml);
    static unsigned getImageSetGridSize(const QString& imageset);

    Grid *getGrid();
    void setGrid(Grid * grid);

    QImage *mosaic() const;
    void setMosaic(QImage *mosaic);

public slots:
    void paint();
    void parseImageSet();
    void processRect(QRect rect);
    void emitProgressValueChanged(int value);
    void emitStarted();
    void emitFinished();
    void resetFields();
    void saveMosaic();

signals:
    void statusChanged(const QString&);
    void progressValueChanged(int value);
    void started(int total);
    void finished();
    void error(QString message);

private:
    QString m_imageset_file;
    QString m_source_image_file;
    QString m_target_image_file;
    QImage *m_mosaic;
    QFutureWatcher<void> *m_future_watcher;
    QList<QRect> *m_tiles;
    Grid *m_grid;
    std::vector<Point> m_points;
    std::vector<ImageSetItem> m_images;
    TreeMap m_trees;

};

#endif // MOSAICPAINTER_H
