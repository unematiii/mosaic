#include <QApplication>

#include "src/ui/NewMosaicDialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    NewMosaicDialog w;
    w.show();

    return a.exec();
}
