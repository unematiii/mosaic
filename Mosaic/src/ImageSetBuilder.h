#ifndef IMAGESETBUILDER_H
#define IMAGESETBUILDER_H

#include <QtConcurrent/QtConcurrent>
#include <QMutex>

class QXmlStreamWriter;

class ImageSetBuilder : public QObject
{
    Q_OBJECT

public:
    ImageSetBuilder(QObject *parent = 0);
    ~ImageSetBuilder();

    void resetFields();
    void cropAndMeasure(QString src, QImage image, QString newsrc);
    void calculateMetrics(QImage image, QString newsrc);

public slots:
    void setFileName(const QString& filename);

    void setImagesDirectory(const QString& directory)
    {
        this->m_images_directory = directory;
    }

    void setOutputDirectory(const QString& directory)
    {
        this->m_output_directory = directory;
    }

    void setCropSize(int size)
    {
        this->m_crop_size = size;
    }

    void setCrop(bool crop)
    {
        this->m_crop = crop;
    }

    void build();
    void processImage(QString src);

    static unsigned calculateAvgColor(const QImage& image);

    void emitProgressValueChanged(int value);
    void emitStarted();
    void emitFinished();

signals:
    void progressValueChanged(int value);
    void started(int total);
    void finished();
    void error(QString message);

private:
    QMutex m_mutex;
    QString m_filename;
    QString m_images_directory;
    QString m_output_directory;
    QStringList m_files_list;

    int m_crop_size;
    bool m_crop;

    QFutureWatcher<void> *m_future_watcher;
    QXmlStreamWriter *m_xml_writer;
    QFile *m_xml_file;
};

#endif // IMAGESETBUILDER_H
