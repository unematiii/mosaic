# Qt >= 5.0

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Mosaic
TEMPLATE = app

# sudo apt-get install libgl-dev
# sudo apt-get install libboost-all-dev
INCLUDEPATH += C:/boost_1_55_0
LIBS += -lQt5Concurrent

SOURCES += src/main.cpp\
    src/ImageSetBuilder.cpp \
    src/MosaicPainter.cpp \
    src/ImagePartitioner.cpp \
    src/ui/ProgessDialog.cpp \
    src/ui/NewMosaicDialog.cpp \
    src/ui/ImageSetWidget.cpp \
    src/ui/AboutDialog.cpp \
    src/misc/Graph.cpp \
    src/misc/Grid.cpp

HEADERS  += \
    src/ImageSetBuilder.h \
    src/ImageSetItem.h \
    src/ImagePartitioner.h \
    src/MosaicPainter.h \
    src/ui/AboutDialog.h \
    src/ui/ImageSetWidget.h \
    src/ui/NewMosaicDialog.h \
    src/ui/ProgessDialog.h \
    src/misc/Graph.h \
    src/misc/Grid.h \
    src/misc/kdtree.h

FORMS    += \
    ImageSetWidget.ui \
    NewMosaicDialog.ui \
    ProgessDialog.ui \
    AboutDialog.ui

CONFIG += c++11
QMAKE_CXXFLAGS += -Wno-unused-local-typedefs -Wno-return-type
RC_FILE = mosaic.rc

RESOURCES += \
    resources.qrc
